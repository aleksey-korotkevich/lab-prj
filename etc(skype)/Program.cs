﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using resultsgenerator.Model;
using System.Net.Http;
using System.Text;
using System.Collections.Generic;
using System.Net;
using System.Net.Http.Headers;

namespace resultsgenerator
{
    class Program
    {
        static Parameters parameters = null;
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("parameters: <config file>");
                return;
            }
            
            try
            {
                using (StreamReader reader = File.OpenText(args[0]))
                {
                    parameters = JsonConvert.DeserializeObject<Parameters>(reader.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to read configuration file. Error {0}.", e.Message);
            }

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(parameters.ResultsHost);
            client.DefaultRequestHeaders.Clear();
            if (parameters.UserName != null && parameters.UserName != string.Empty)
            {
                var byteArray = Encoding.ASCII.GetBytes(string.Format("{0}:{1}", parameters.UserName, parameters.Password));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            }

            try
            {
                var httpContent = new StringContent("", Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PutAsync("results", httpContent).GetAwaiter().GetResult();
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Failed to add search index. Responce:{0}. Will try to add items to the existing index.", response.StatusCode);
                }
                else
                {
                    Console.WriteLine("Index is created.");
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Failed to add search index. Exception:{0}.", e.Message);
                return;
            }

            Dictionary<string, int> testNumber = new Dictionary<string, int>();
            foreach (string cell in parameters.TestCell)
            {
                testNumber.Add(cell, 0);
            }

            int resultsCount = 0;

            DateTime startTime = parameters.StartSimulationInterval;
            startTime = new DateTime(startTime.Year, startTime.Month, startTime.Day, parameters.ShiftStartTime.Hours, parameters.ShiftStartTime.Minutes, 0);

            DateTime testTime = startTime;
            for (int m = 0; m < 12; m++) // max 1 year
            {
                if (testTime > parameters.StopSimulationInterval)
                {
                    break;
                }
                for (int w = 0;w < 4;w++) // 4 weeks per month
                {
                    if (testTime > parameters.StopSimulationInterval)
                    {
                        break;
                    }

                    for (int d = 0; d < 7; d++) // 7 days per week
                    {
                        if (testTime > parameters.StopSimulationInterval)
                        {
                            break;
                        }

                        foreach (string cell in parameters.TestCell)
                        {
                            testTime = new DateTime(parameters.StartSimulationInterval.Year, parameters.StartSimulationInterval.Month, parameters.StartSimulationInterval.Day, parameters.ShiftStartTime.Hours, parameters.ShiftStartTime.Minutes, 0).Add(new TimeSpan(d + w * 7 + m * 30, 0, 0, 0));

                            for (int t = 0; t < 20; t++) // up to 20 tests per day
                            {
                                Random rnd = new Random();
                                testTime = testTime.Add(new TimeSpan(rnd.Next(parameters.MinTestTime, parameters.MaxTestTime), 0, 0));
                                if (testTime.Hour > parameters.ShiftEndTime.Hours)
                                {
                                    // no tests after shift
                                    break;
                                }

                                ResultDescrption res = GenerateResult(testTime, cell, ++testNumber[cell]);

                                var stringPayload = JsonConvert.SerializeObject(res);
                                var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                                var response = client.PostAsync("/results/result/", httpContent).GetAwaiter().GetResult();
                                if (!response.IsSuccessStatusCode)
                                {
                                    Console.WriteLine("Failed to add test result. Responce:{0}", response.StatusCode);
                                }
                                else
                                {
                                    resultsCount++;
                                }
                                testTime = testTime.Add(new TimeSpan(rnd.Next(parameters.MinPauseTime, parameters.MaxPauseTime), 0, 0));
                            }
                        }
                    }
                }
            }

            Console.WriteLine("{0} results generated for {1} test cells.", resultsCount, testNumber.Count);
            Console.ReadKey();
        }

        static public ResultDescrption GenerateResult(DateTime startTime, string testCell, int testNumber)
        {
            ResultDescrption result = new ResultDescrption();
            result.StartTime = startTime;
            result.TestCell = testCell;
            Random rnd = new Random();
            result.Name = string.Format("CCC-{0}-yyyy-{1}-Result", testCell, testNumber);
            result.TestNumber = testNumber.ToString();
            result.Driver = parameters.Driver[rnd.Next(0, parameters.Driver.Count)];
            result.Operator = parameters.Operator[rnd.Next(0, parameters.Operator.Count)];
            result.TestName = parameters.TestName[rnd.Next(0, parameters.TestName.Count)];
            result.Requestor = parameters.Requestor[rnd.Next(0, parameters.Requestor.Count)];
            result.VehicleMaker = parameters.VehicleMaker[rnd.Next(0, parameters.VehicleMaker.Count)];
            result.EngineID = rnd.Next(100000, 99999999).ToString();
            result.VIN = rnd.Next(100000, 99999999).ToString();
            result.Fail = rnd.Next(0,100) < parameters.FailProbability;
            return result;
        }
    }
}