const parseXlsx = require('excel');
const fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');

const targetDir = 'generated';
const initDir = path.isAbsolute(targetDir) ? path.sep : '';

let rowWithTitles = [];

parseXlsx(path.join(__dirname, process.argv[2]), (err, data) => {
    if (err) {
        throw err;
    }

    let trimmed = data.filter(d => {
        return !d.every(e => e === '');
    });

    trimmed = trimmed.map((d, i) => {
        if (i === 0) {
            rowWithTitles = [...d];

            return d;
        } else {
            return parseLine(d);
        }
    });

    trimmed[0] = trimmed[0].filter(d => d !== '');
    trimmed[0] = trimmed[0].map(splitCamelize);

    let result = reduceToJsonWithFields(trimmed);

    saveJsonToFile(result);

    saveEachCarSeparately(result);
});

function parseLine(line) {
    return line.filter((l, i) => {
        return rowWithTitles[i] !== '';
    });
}

function splitCamelize(str) {
    let newStr = str.replace(/_/g, ' ');
    let splitted = newStr.split(' ');

    if(splitted.length === 1) {
        return str;
    } else {
        splitted = splitted.map(s => s.toLowerCase()).map(s => `${s.charAt(0).toUpperCase()}${s.slice(1)}`);

        return splitted.join('');
    }
}

function reduceToJsonWithFields(array) {
    array = array.reduce((acc, value, idx) => {
        if (idx !== 0) {
            let json = value.reduce((obj, f, i) => {
                obj[array[0][i]] = f;

                return obj;
            }, {});

            acc.push(json);
        }

        return acc;
    }, []);

    return array;
}

function saveJsonToFile(data, path = `${process.argv[2]}.json`) {
    fs.writeFile(path, JSON.stringify(data, null, 4), () => {
        console.log(`File ${path} saved!`);
    });
}

function cleanGenerated(cb) {
    fs.exists(`${targetDir}${path.sep}fords`, (res) => {
        if(res) {
            rimraf(`${targetDir}${path.sep}fords`, () => {
                cb();
            });
        } else {
            cb();
        }
    });
}

function saveEachCarSeparately(data) {
    cleanGenerated(() => {
        `${targetDir}${path.sep}fords`.split(path.sep).reduce((parentDir, childDir) => {
            const curDir = path.resolve(parentDir, childDir);

            if (!fs.existsSync(curDir)) {
                fs.mkdirSync(curDir);
            }

            return curDir;
        }, initDir);

        data.forEach(d => {
            let vehicle = {
                DisplayName: `Ford ${d.Modell}`,
                ...d
            };

            fs.writeFile(`${targetDir}/fords/${vehicle.DisplayName}-${vehicle.VIN}.json`, JSON.stringify(vehicle, null, 4), () => {
                console.log(`Car ${vehicle.DisplayName} saved!`);
            });
        });
    });
}